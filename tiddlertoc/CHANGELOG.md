# Changelog

## [0.0.3] — 2023-09-14

### Fixed

-  Actually increase the plugin's version number.

## [0.0.2] — 2023-09-14

### Added

-  Allow table of contents to be collapsed.

## [0.0.1] — 2023-09-10

_Initial release._

[0.0.3]: https://gitlab.com/goeb/tiddlywiki-plugins/-/tree/tt-0.0.3/tiddlertoc?ref_type=tags
[0.0.2]: https://gitlab.com/goeb/tiddlywiki-plugins/-/tree/tt-0.0.2/tiddlertoc?ref_type=tags
[0.0.1]: https://gitlab.com/goeb/tiddlywiki-plugins/-/tree/tt-0.0.1/tiddlertoc?ref_type=tags
