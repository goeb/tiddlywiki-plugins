# TiddlerToC plugin for TiddlyWiki

The **TiddlerToC** plugin for [TiddlyWiki][1] provides support for
single-tiddler heading-based tables of contents.

This repository contains the plugin sources and examples, usable with a
[Node.js TiddlyWiki][2] setup. See <https://tiddlertoc.tiddlyhost.com/>
for a live (single-file) version with usage examples, that can be used
to [install][3] the plugin in another single-file wiki.

## Building

The plugin itself can be found in the `plugin/` directory. To build a
single-file wiki including the plugin, run:

```sh
tiddlywiki plugin/ --build
```

After that, `plugin.html` can be found in the `output/` directory.

To use the plugin with an existing Node.js-based TiddlyWiki, copy the
`plugin/plugins` directory to the wiki's root.

The `examples/` directory contains the usage examples, and the `test/`
directory the plugin's unit tests. Single-file wikis can be built for
these in the same way as for the actual plugin. To run the tests from
the command line:

```sh
tiddlywiki test/ --test
```

## License

Copyright © 2023 Stefan Göbel « tiddly ʇɐ subtype ˙ de »

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.

[1]: https://tiddlywiki.com/
[2]: https://tiddlywiki.com/#TiddlyWiki%20on%20Node.js
[3]: https://tiddlywiki.com/#Manually%20installing%20a%20plugin
