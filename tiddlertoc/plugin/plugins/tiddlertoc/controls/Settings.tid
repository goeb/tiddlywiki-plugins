caption: TiddlerToC
tags:    $:/tags/ControlPanel/SettingsTab
title:   $:/plugins/gœb/TiddlerToC/controls/Settings
type:    text/vnd.tiddlywiki

\define ttoc-text(fld,def,dis)
   <$let
      s="$:/plugins/gœb/TiddlerToC/state"
      f="TToC-$fld$"
   >
      <div class="ttoc-ctrl-wrapper">
         <div class="ttoc-ctrl-input">
            <$set
               name="notset"
               filter="[<s>has:field<f>]"
               value="""<div class="ttoc-indicator"></div>"""
               emptyValue=""
            ><<notset>></$set>
            <$edit-text
               tag="input"
               class="tc-max-width"
               tiddler=<<s>>
               field=<<f>>
               placeholder="$def$"
            />
         </div>
         <div class="ttoc-ctrl-buttons">
            <$set
               name="eqdef"
               filter="[<s>has:field<f>then<s>field:TToC-$fld$[$def$]]"
               value="eqdef"
               emptyValue="neqdef"
            >
               <$set
                  name="notset"
                  filter="[<s>has:field<f>]"
                  value="no"
                  emptyValue="yes"
               >
                  <$button disabled="$dis$">
                     <$action-setfield
                        $tiddler=<<s>>
                        $field=<<f>>
                        $value=""
                     />
                     ∅
                  </$button>
                  <$button disabled=<<notset>> class=<<eqdef>>>
                     <$action-deletefield
                        $tiddler=<<s>>
                        $field=<<f>>
                     />
                     ↺
                  </$button>
               </$set>
            </$set>
         </div>
      </div>
   </$let>
\end

\define ttoc-checkbox(fld,def,lbl)
   \define set(v)
      <$action-setfield $tiddler=<<s>> $field=<<f>> $value="$v$"/>
   \end set
   <$let
      s="$:/plugins/gœb/TiddlerToC/state"
      f="TToC-$fld$"
   >
      <div class="ttoc-ctrl-wrapper">
         <div class="ttoc-ctrl-input ttoc-ctrl-input-checkbox">
            <$set name="refreshTrigger" tiddler=<<s>> field=<<f>>>
               <$checkbox
                  filter="[<s>get<f>else[unknown]]"
                  checked="yes"
                  unchecked="no"
                  indeterminate="yes"
                  checkactions=<<set yes>>
                  uncheckactions=<<set no>>
               >
                  $lbl$
               </$checkbox>
            </$set>
         </div>
         <div class="ttoc-ctrl-buttons">
            <$set
               name="notset"
               filter="[<s>has:field<f>]"
               emptyValue="yes"
               value="no"
            >
            <$set
               name="eqdef"
               filter="[<s>has:field<f>then<s>field:TToC-$fld$[$def$]]"
               emptyValue="neqdef"
               value="eqdef"
            >
               <$button disabled="yes">∅</$button>
               <$button disabled=<<notset>> class=<<eqdef>>>
                  <$action-deletefield
                     $tiddler=<<s>>
                     $field=<<f>>
                  />
                  ↺
               </$button>
            </$set>
            </$set>
         </div>
      </div>
   </$let>
\end

\define ttoc-select(fld,val,def)
   <$let
      s="$:/plugins/gœb/TiddlerToC/state"
      f="TToC-$fld$"
   >
      <div class="ttoc-ctrl-wrapper">
         <div class="ttoc-ctrl-input">
            <$select
               tiddler=<<s>>
               field=<<f>>
               default="Default: $def$"
               class="tc-max-width"
            >
               <option disabled>Default: $def$</option>
               <$list filter="[[$val$]split[ ]]" variable="opt">
                  <option><<opt>></option>
               </$list>
            </$select>
         </div>
         <div class="ttoc-ctrl-buttons">
            <$set
               name="notset"
               filter="[<s>has:field<f>]"
               emptyValue="yes"
               value="no"
            >
            <$set
               name="eqdef"
               filter="[<s>has:field<f>then<s>field:TToC-$fld$[$def$]]"
               emptyValue="neqdef"
               value="eqdef"
            >
               <$button disabled="yes">∅</$button>
               <$button disabled=<<notset>> class=<<eqdef>>>
                  <$action-deletefield
                     $tiddler=<<s>>
                     $field=<<f>>
                  />
                  ↺
               </$button>
            </$set>
            </$set>
         </div>
      </div>
   </$let>
\end

\define reset-all()
   <$let ttstate="$:/plugins/gœb/TiddlerToC/state">
      <$set
         name="disabled"
         filter="[<ttstate>has:field[title]]"
         emptyValue="yes"
         value="no"
      >
         <$button class="tc-max-width" disabled=<<disabled>>>
            <$action-deletetiddler $tiddler=<<ttstate>>/>
            ↺ Reset all options to their default values
         </$button>
      </$set>
   </$let>
\end

\define excl() $(x1)$,$(x2)$,$(x3)$,$(x4)$

The following controls change the defaults of the `<$tiddlertoc/>`
widget. All settings can be overridden for an individual widget using
the respective attributes.

For settings marked as //pseudo-selector//, the basic format is
`[<tag>][.<class>]…`, that's zero or one tag, followed by zero or more
class selectors (each starting with a dot followed by the class name).
Multiple of these can be separated by comma to match multiple elements,
for example, `h1,div.foo.bar`. No other attributes (`#<ID>`…) or
selectors are supported. Values must not include spaces. An empty
selector will never match anything.

For settings marked as //boolean//, truthy values are `enable`,
`enabled`, `on`, `true` and `yes` - case doesn't matter. All other
values (including an empty string) will be interpreted as false.

---

Use the ''∅'' buttons to set a value to an empty string (same as
entering some text and deleting it - enabled only where it makes sense).

Use the ''↺'' buttons to reset the respective option to its default, or
use the button below to reset all options to the default values
(warning: this will delete all customizations of the options below,
there is no undo).

<<reset-all>>

---

@@.cfg ''Table of contents title''@@ (attribute: `title`). The title at
the top of the table of contents. Defaults to `Table of contents`. Can
be set to an empty string to not add a title element at all. Note that
clicking the title collapses or expands the title of contents, and won't
be possible if the title is empty.

<<ttoc-text "title" "Table of contents">>

@@.cfg ''Table of contents classes''@@ (attribute: `class`).
Space-separated list of classes to add to the main `<div>` container of
the table of contents. The class `tiddler-toc` will always be added. By
default this option is empty.

<<ttoc-text "class" "">>

@@.cfg ''Add heading classes to table of contents''@@ (attribute:
`addClasses`, //boolean//). If truthy, the classes of the heading
element (for example, classes of an `h1` element in the tiddler) will be
added to the respective element in the table of contents. Defaults to
`yes`.

<<ttoc-checkbox
   "addClasses"
   "yes"
   "Add the headings' classes to the table of contents."
>>

---

@@.cfg ''Included headings''@@ (attribute: `h1`…`h6`,
//pseudo-selector//). These selectors will be used to find the headings
in the tiddler. The default values for the six levels of headings are
`h1`…`h6`, same as the attribute names. Note that if multiple selectors
match one heading, its level will be set to the highest (the numerically
lowest) one. Empty strings will turn off the respective headings.

<<ttoc-text "h1" "h1">>
<<ttoc-text "h2" "h2">>
<<ttoc-text "h3" "h3">>
<<ttoc-text "h4" "h4">>
<<ttoc-text "h5" "h5">>
<<ttoc-text "h6" "h6">>

@@.cfg ''Ignored headings''@@ (attribute: `ignore`,
//pseudo-selector//). Headings that match this pseudo-selector won't be
included in the table of contents. The default value is `.toc-ignore`.

<<ttoc-text "ignore" ".toc-ignore">>

@@.cfg ''Heading IDs''@@ (attribute: `useIDs`, either `yes`, `no` or
`force`). If `yes` (the default), anchor links will be created in the
table of contents (instead of `span`s with a JavaScript handler) for all
headings with an `id` attribute. If `no`, `id` attributes will be
ignored. If set to `force`, the table of contents will only include
headings with an `id` attribute (using anchor links for navigation).

<<ttoc-select "useIDs" "yes no force" "yes">>

---

@@.cfg ''Main tiddler container''@@ (attribute: `tiddlerBody`,
//pseudo-selector//). The outer container of the tiddler contents, in
which the widget will look for headings when navigating using the
JavaScript handler (outside of excluded containers, see below). The
default, `div.tc-tiddler-body`, should only be changed if you know what
you're doing.

<<ttoc-text "tiddlerBody" "div.tc-tiddler-body">>

@@.cfg ''Excluded containers''@@ (attribute: `exclude`,
//pseudo-selector//). Defines the container elements that will be
skipped in a tiddler when gathering the headings (that is, headings
within such containers won't be included in the table of contents). The
default value excludes the content of TiddlyWiki's `<<tabs>>` and
`<<toc-tabbed-external-nav>>`/`<<toc-tabbed-internal-nav>>` macros, as
well as the `<$reveal/>` widget. Note that for tiddlers //inside// tabs,
if they contain the `<$tiddlertoc/>` widget, the table of contents will
be rendered unless turned off by other means (this only applies to the
`<<tabs>>`). The default selector contains the following elements:

* `div.tc-tab-set`
* `div.tc-tabbed-table-of-contents`
* `div.tc-reveal`
* `span.tc-reveal`

<$let
   x1="div.tc-tab-set"
   x2="div.tc-tabbed-table-of-contents"
   x3="div.tc-reveal"
   x4="span.tc-reveal"
><$macrocall $name="ttoc-text" fld="exclude" def=<<excl>>/></$let>

---

@@.cfg ''Check current and story tiddler''@@ (attribute: `checkStory`,
//boolean//). If this is set to a truthy value, no table of contents
will be generated if the `currentTiddler` variable doesn't equal the
`storyTiddler` variable. This defaults to `yes`, and shouldn't be
changed unless you know what you're doing. Note that turning this off
will use the `currentTiddler` instead of the `storyTiddler` as the
source tiddler for the table of contents. A more detailed explanation on
how the source tiddler is selected can be found in the plugin's
[[Readme|$:/plugins/gœb/TiddlerToC/docs/Readme]] tiddler.

<<ttoc-checkbox
   "checkStory"
   "yes"
   "Check that `currentTiddler` matches `storyTiddler`."
>>

@@.cfg ''Disable table of contents using a variable''@@ (attribute:
`skipVar`). If a variable by the configured name, which defaults to
`noToC`, is set to a truthy value, no table of contents will be created.

<<ttoc-text "skipVar" "noToC" "yes">>

@@.cfg ''Disable table of contents with tiddler fields''@@. The
following settings define the names of fields used to turn off the table
of contents if set to a truthy value. The widget will look for such a
field in four tiddlers: the `currentTab`, `currentTiddler`,
`storyTiddler`, and `thisTiddler`&thinsp;—&thinsp;these are variable
names, not tiddler titles&thinsp;—&thinsp;and if one of these contains
its respective field set to a truthy value, no table of contents will be
generated.

For `currentTab` (attribute: `skipTab`), default: `noTabToC`:

<<ttoc-text "skipTab" "noTabToC" "yes">>

For `currentTiddler` (attribute: `skipCurrent`), default:
`noCurrentToC`:

<<ttoc-text "skipCurrent" "noCurrentToC" "yes">>

For `storyTiddler` (attribute: `skipStory`), default: `noStoryToC`:

<<ttoc-text "skipStory" "noStoryToC" "yes">>

For `thisTiddler` (attribute: `skipThis`), default: `noThisToC`:

<<ttoc-text "skipThis" "noThisToC" "yes">>

---

@@.cfg ''Refresh behaviour''@@ (attribute: `refresh`, either `default`
or `relaxed`). If set to `default` (the default), the widget will try to
get a list of all transcluded tiddlers and the table of contents will be
refreshed on any change in any of these tiddlers. This might be slow, so
the `relaxed` setting disables this behaviour (the widget will still be
refreshed when source tiddler changes, as well as whenever the default
settings are modified).

<<ttoc-select "refresh" "default relaxed" "default">>

---

@@.cfg ''scrollIntoView parameters''@@. The following values will be
passed to the `scrollIntoView()` method when an entry in the table of
contents is triggered (clicked, …) to scroll to the heading (doesn't
apply to headings with an `id` attribute). For more details, see the
documentation at
[[https://developer.mozilla.org/docs/Web/API/Element/scrollIntoView]].

The `behaviour` parameter (attribute: `scrollBehaviour`), default:
`smooth`:

<<ttoc-select "scrollBehaviour" "smooth instant auto" "smooth">>

The `block` parameter (attribute: `scrollBlock`), default: `start`:

<<ttoc-select "scrollBlock" "start center end nearest" "start">>

The `inline` parameter (attribute: `scrollInline`), default: `nearest`:

<<ttoc-select "scrollInline" "start center end nearest" "nearest">>
