(function () {

   /* global $tw: false */

   "use strict";

   // Default values for settings and attributes. These can be changed
   // globally in the control panel, as well as for individual widgets
   // using the widget attributes (same name as the keys).
   //
   // Remember to add or remove fields or change the placeholder text in
   // the configuration tiddler when modifying the defaults. Also update
   // the Readme.

   const defaults = {
      "addClasses"     : "yes",                 // add heading classes
      "checkStory"     : "yes",                 // storyT. = currentT.?
      "class"          : "",                    // main div classes
      "ignore"         : ".toc-ignore",         // ignored headings
      "refresh"        : "default",             // "default", "relaxed"
      "scrollBehaviour": "smooth",              // scrollIntoView params
      "scrollBlock"    : "start",               // 
      "scrollInline"   : "nearest",             // 
      "skipCurrent"    : "noCurrentToC",        // currenTiddler field
      "skipStory"      : "noStoryToC",          // storyTiddler  
      "skipTab"        : "noTabToC",            // currentTab    
      "skipThis"       : "noThisToC",           // thisTiddler   
      "skipVar"        : "noToC",               // no ToC variable name
      "tiddlerBody"    : "div.tc-tiddler-body", // main tiddler wrapper
      "title"          : "Table of contents",   // ToC title
      "useIDs"         : "yes",                 // "yes", "no", "force"
   };

   // Ignore these containers when gathering the headings.
   //
   defaults.exclude = [
      "div.tc-tab-set",
      "div.tc-tabbed-table-of-contents",
      "div.tc-reveal",
      "span.tc-reveal",
   ].join (",");

   // Use six levels of headings (just because it matches `h1`…`h6`).
   //
   const headingLevels = 6;
   for (let level = 1; level <= headingLevels; level += 1) {
      defaults [`h${level}`] = `h${level}`;
   }

   // Some more constants…
   //
   const attrCollapsed  = "data-collapsed";
   const configTitle    = "$:/plugins/gœb/TiddlerToC/state";
   const configFieldPfx = "TToC-";
   const divClass       = "tiddler-toc";
   const noRecursionVar = "PreventTiddlerToCFromRecursionPlease";
   const noRecursion    = { "variables": { [noRecursionVar]: "1" } };
   const headingClass   = "toc-heading";
   const truthy         = ["enable", "enabled", "on", "true", "yes"];

   // A Selector used to match TiddlyWiki's fakeDOM elements.
   //
   class Selector {

      // Create a new Selector instance. The `selector` must be a string
      // in the format "[<tag>][.<class>]…", an empty string disabled
      // the selector. The (optional) `level` specifies the level of the
      // heading this selector matches.
      //
      constructor (selector, level) {

         this.classes = [];
         this.enabled = true;
         this.level   = typeof level === "number" ? level : null;
         this.tag     = "";

         if (typeof selector !== "string" || selector === "") {
            this.enabled = false;
            return;
         }

         const split  = selector.split (".");

         this.classes = split.slice (1);
         this.tag     = split [0].toLowerCase ();

      }

      // Return whether this selector matches the specified parse tree
      // `node` (`true` or `false`). A disabled selector never matches.
      //
      match (node) {

         if (! this.enabled) {
            return false;
         }

         if (this.tag !== "") {
            if (this.tag !== node?.tagName?.toLowerCase ()) {
               return false;
            }
         }

         const classAttr = node?.attributes?.class ?? "";

         const classes = classAttr.split (" ").filter (
            (cls) => { return cls !== ""; },
         );

         return this.classes.every (
            (cls) => { return classes.indexOf (cls) !== -1; },
         );

      }

   }

   // Widget stuff.

   const Widget = require ("$:/core/modules/widgets/widget.js").widget;

   // Initialize the widget.
   //
   const ToC = function (parseTreeNode, options) {

      this.initialise (parseTreeNode, options);

      this.allTiddlers = [];
      this.config      = {};
      this.headings    = [];
      this.skipToC     = false;
      this.tocTiddler  = null;

   };

   ToC.prototype = new Widget ();

   // Get the widget configuration from the defaults, the settings
   // tiddler and/or the widget attributes (later ones overriding
   // previous ones).
   //
   ToC.prototype.configure = function () {

      const configTiddler = this.wiki.getTiddler (configTitle);
      let   defaultValue  = null;

      if (typeof configTiddler === "undefined") {
         defaultValue = function (attr) {
            return defaults [attr];
         };
      }
      else {
         defaultValue = function (attr) {
            const fld = configTiddler.fields?.[configFieldPfx + attr];
            return typeof fld === "undefined" ? defaults [attr] : fld;
         };
      }

      for (const attr of Object.keys (defaults)) {
         this.config [attr] = this.getAttribute (
            attr,
            defaultValue (attr),
         ).trim ();
      }

      this.ignoreHeadings = [];
      if (this.config.ignore !== "") {
         for (const sel of this.config.ignore.split (",")) {
            this.ignoreHeadings.push (new Selector (sel));
         }
      }

      this.excludeElements = [];
      if (this.config.exclude !== "") {
         for (const sel of this.config.exclude.split (",")) {
            this.excludeElements.push (new Selector (sel));
         }
      }

      this.hSelectors = [];
      for (let level = 1; level <= headingLevels; level += 1) {
         if (this.config [`h${level}`] !== "") {
            for (const sel of this.config [`h${level}`].split (",")) {
               this.hSelectors.push (new Selector (sel, level));
            }
         }
      }

   };

   // Return `true` if the `field` of the tiddler with the specified
   // `title` is set to a truthy value, `false` otherwise. Also return
   // `false` if the tiddler doesn't exist, or `title` isn't a non-empty
   // string.
   //
   ToC.prototype.isFieldTruthy = function (title, field) {

      if (typeof title !== "string" || title === "") {
         return false;
      }

      if (typeof field !== "string" || field === "") {
         return false;
      }

      const tiddler = this.wiki.getTiddler (title);
      const value   = tiddler?.fields?.[field];

      return truthy.includes (value?.toLowerCase ());

   };

   // Return `true` if any of the variables that should prevent ToC
   // generation is set to a truthy value, or if the current tiddler is
   // not the `storyTiddler` (unless this condition is turned off),
   // `false` otherwise.
   //
   ToC.prototype.shouldSkip = function () {

      if (this.getVariable (noRecursionVar)) {
         return true;
      }

      const skipVar = this.getVariable (this.config.skipVar);
      if (truthy.includes (skipVar?.toLowerCase ())) {
         return true;
      }

      const currentTab     = this.getVariable ("currentTab"    );
      const currentTiddler = this.getVariable ("currentTiddler");
      const storyTiddler   = this.getVariable ("storyTiddler"  );
      const thisTiddler    = this.getVariable ("thisTiddler"   );

      if (truthy.includes (this.config.checkStory.toLowerCase ())) {
         if (storyTiddler !== currentTiddler) {
            return true;
         }
      }

      const conf = this.config;

      if (
         this.isFieldTruthy (currentTiddler, conf.skipCurrent) ||
         this.isFieldTruthy (storyTiddler,   conf.skipStory  ) ||
         this.isFieldTruthy (currentTab,     conf.skipTab    ) ||
         this.isFieldTruthy (thisTiddler,    conf.skipThis   )
      ) {
         return true;
      }

      return false;

   };

   // Set the source tiddler for the table of contents and return true,
   // or return false if it can't be determined. Source tiddler order of
   // precedence is widget attribute, `currentTab`, `storyTiddler` (in
   // that order, first one set wins), unless the `checkStory` setting
   // is turned off, in which case `currentTiddler` will be checked
   // before `storyTiddler`.
   //
   ToC.prototype.setSourceTiddler = function () {

      let tiddler = null;

      if (truthy.includes (this.config.checkStory.toLowerCase ())) {
         tiddler =
            this.getAttribute ("tiddler"       ) ??
            this.getVariable  ("currentTab"    ) ??
            this.getVariable  ("storyTiddler"  );
      }
      else {
         tiddler =
            this.getAttribute ("tiddler"     ) ??
            this.getVariable  ("currentTab"  ) ??
            this.getVariable  ("currentTiddler") ??
            this.getVariable  ("storyTiddler");
      }

      if (typeof tiddler === "undefined" || tiddler === "") {
         return false;
      }

      this.tocTiddler = tiddler;

      return true;

   };

   // Gather a list of transcluded tiddlers of the specified parse tree
   // `node` and its children and add it to the `allTiddlers` list of
   // the widget. Tiddlers found will be checked recursively. Note that
   // tiddlers that don't exist will be added to the list, too.
   //
   ToC.prototype.nodeTranscludes = function (node) {

      for (const name of ["tiddler", "$tiddler"]) {

         const v = node?.attributes?.[name]?.value;

         if (typeof v === "string" && ! this.allTiddlers.includes (v)) {
            this.allTiddlers.push (v);
            const tid = this.wiki.parseTiddler (v, noRecursion);
            if (Array.isArray (tid?.tree)) {
               for (const child of tid.tree) {
                  this.nodeTranscludes (child);
               }
            }
         }

      }

      if (Array.isArray (node?.children)) {
         for (const child of node.children) {
            this.nodeTranscludes (child);
         }
      }

   };

   // Find tiddlers directly or indirectly transcluded by the tiddler
   // for which the ToC is generated and add these to the `allTiddlers`
   // list of the widget.
   //
   ToC.prototype.findTranscluded = function () {

      const tid = this.wiki.parseTiddler (this.tocTiddler, noRecursion);

      if (Array.isArray (tid?.tree)) {
         for (const node of tid.tree) {
            this.nodeTranscludes (node);
         }
      }

   };

   // As `$tw.wiki.parseTiddler()`, but explicitly imports all the
   // variables (as the view template would do) and skips the cache.
   //
   ToC.prototype.parseTiddler = function (title) {

      const tiddler = this.wiki.getTiddler (title);

      if (! tiddler) {
         return null;
      }

      const opts = {};
      const curi = "_canonical_uri";

      if (tiddler.hasField (curi)) {
         opts [curi] = tiddler.fields [curi];
      }

      const flt = "[subfilter{$:/core/config/GlobalImportFilter}]";
      const txt = `\\import ${flt}\n\n${tiddler.fields.text}`;

      return this.wiki.parseText (tiddler.fields.type, txt, opts);

   };

   // Get all headings from the specified list of `nodes`, recursively.
   // The `counter` parameter must be an array of zeroes of the size
   // `headingLevels` + 1 when this method is initially called (first
   // element will be ignored). The headings found will be added to the
   // `headings` list of the widget.
   //
   ToC.prototype.extractHeadings = function (nodes, counter) {

      const that = this;
      const useIDs = this.config.useIDs.toLowerCase ();

      const filter = function (node) {
         return "tagName" in node && ! that.excludeElements.some (
            (sel) => { return sel.match (node); },
         );
      };

      const ignore = function (node) {
         return that.ignoreHeadings.some (
            (sel) => { return sel.match (node); },
         );
      };

      const include = function (txt, id) {
         return txt !== "" && ! (id === "" && useIDs === "force");
      };

      for (const node of nodes.filter (filter)) {

         let matched = false;

         for (const selector of this.hSelectors) {

            if (selector.match (node) && ! ignore (node)) {

               const txt = node.textContent.trim ();
               const id  = (node.getAttribute ("id") ?? "").trim ();

               if (include (txt, id)) {

                  counter [selector.level] += 1;
                  matched                   = true;

                  const cl = (node.getAttribute ("class") ?? "")
                     .trim ()
                     .replace (/\s\s+/gu, " ");

                  this.headings.push ({
                     "classes": cl,
                     "counter": counter [selector.level],
                     "id"     : id,
                     "level"  : selector.level,
                     "text"   : node.textContent.trim (),
                  });

                  break;

               }

            }

         }

         if (! matched && Array.isArray (node?.children)) {
            this.extractHeadings (node.children, counter);
         }

      }

   };

   // Find all headings in the source tiddler and add them to the
   // `headings` list of the widget.
   //
   ToC.prototype.findHeadings = function () {

      const noRec = $tw.utils.deepCopy  (noRecursion);
      const vars  = noRec.variables;

      vars.currentTab     = this.getVariable ("currentTab"    ) ?? "";
      vars.currentTiddler = this.getVariable ("currentTiddler") ?? "";
      vars.storyTiddler   = this.getVariable ("storyTiddler"  ) ?? "";
      vars.thisTiddler    = this.getVariable ("thisTiddler"   ) ?? "";
      vars.transclusion   = this.getVariable ("transclusion"  ) ?? "";

      const tmpl   = $tw.fakeDocument.createElement ("template");
      const parser = this.parseTiddler (this.tocTiddler);

      if (! parser) {
         return;
      }

      this.wiki.makeWidget (parser, noRec).render (tmpl, null);

      this.headings = [];

      if (Array.isArray (tmpl.children)) {

         const counter = [null];
         for (let i = 1; i <= headingLevels; i += 1) {
            counter [i] = 0;
         }

         this.extractHeadings (tmpl.children, counter);

      }

   };

   // Set the widget properties.
   //
   ToC.prototype.execute = function () {

      this.configure ();

      if (this.shouldSkip ()) {
         this.skipToC = true;
         return;
      }

      if (! this.setSourceTiddler ()) {
         this.skipToC = true;
         return;
      }

      this.allTiddlers = [this.tocTiddler, configTitle];
      if (this.config.refresh === "default") {
         this.findTranscluded ();
      }

      this.findHeadings ();

      // Does nothing.
      //
      this.makeChildWidgets ();

   };

   // Return an event handler to scroll to the specified `heading`.
   //
   ToC.prototype.makeHandler = function (heading) {

      const behaviour = this.config.scrollBehaviour;
      const block     = this.config.scrollBlock;
      const inline    = this.config.scrollInline;

      const wrapper   = this.config.tiddlerBody;
      const entries   = `div.${divClass} > .${headingClass}`;
      const exclude   = this.config.exclude;
      const ignore    = this.config.ignore;
      const query     = this.config [`h${heading.level}`];

      const nodeIndex = heading.counter - 1;

      return function (event) {

         const body  = event.target.closest (wrapper);
         const excl  = event.target.closest (exclude);

         // Get all headings (of the `heading`'s level) either from an
         // excluded container if this ToC is inside one (closest `excl`
         // is set in this case) or from the main tiddler body container
         // otherwise.
         //
         const nodes = Array.from (
            (excl || body).querySelectorAll (query),
         )

            // Now remove all the elements that are in a nested excluded
            // container (if the closest isn't the one found earlier, or
            // none was found earlier and the heading is inside one).
            //
            .filter ((e) => { return e.closest (exclude) === excl; })

            // Just in case spans are headings, remove the entries in
            // the table of contents.
            //
            .filter ((e) => { return ! e.matches (entries); })

            // And remove all elements that are ignored.
            //
            .filter ((e) => { return ! e.matches (ignore); })

            // And all empty headings.
            //
            .filter ((e) => { return e.textContent.trim () !== ""; });

         nodes [nodeIndex].scrollIntoView ({
            "behaviour": behaviour,
            "block"    : block,
            "inline"   : inline,
         });

      };

   };

   // Return an entry (as a DOM node) for the table of contents based on
   // the `heading`.
   //
   ToC.prototype.makeHeading = function (heading) {

      const useIDs = this.config.useIDs.toLowerCase ();
      const text   = this.document.createTextNode (heading.text);

      let classes = `${headingClass} ${headingClass}-${heading.level}`;
      if (truthy.includes (this.config.addClasses.toLowerCase ())) {
         classes = `${classes} ${heading.classes}`.trim ();
      }

      let hElem = null;

      if (heading.id === "" || useIDs === "no") {
         hElem = this.document.createElement ("span");
         hElem.addEventListener ("click", this.makeHandler (heading));
      }
      else {
         hElem = this.document.createElement ("a");
         hElem.setAttribute ("href", `#${heading.id}`);
      }

      hElem.appendChild (text);

      hElem.setAttribute ("class", classes);
      hElem.setAttribute ("title", heading.text);

      return hElem;

   };

   // Render the widget.
   //
   ToC.prototype.render = function (parent, nextSibling) {

      this.computeAttributes ();
      this.execute ();

      if (this.skipToC) {
         return;
      }

      // Required for `refreshSelf()`.
      //
      this.parentDomNode = parent;

      const div = this.document.createElement ("div");
      const cls = `${divClass} ${this.config.class}`.trim ();

      div.setAttribute ("class", cls);

      if (this.config.title !== "" && this.headings.length > 0) {

         const ttl = this.document.createElement  ("span");
         const txt = this.document.createTextNode (this.config.title);

         const hcl = `${headingClass} ${headingClass}-0`;

         ttl.setAttribute ("class", hcl);
         ttl.setAttribute ("title", this.config.title);

         ttl.addEventListener ("click", (event) => {
            const toc = event.target.closest (`div.${divClass}`);
            if (toc.hasAttribute (attrCollapsed)) {
               toc.removeAttribute (attrCollapsed);
            }
            else {
               toc.setAttribute (attrCollapsed, "yes");
            }
         });

         ttl.appendChild (txt);
         div.appendChild (ttl);

      }

      for (const heading of this.headings) {
         div.appendChild (this.makeHeading (heading));
      }

      parent.insertBefore (div, nextSibling);
      this.renderChildren (div, null);
      this.domNodes.push  (div);

   };

   // Re-render the widget if required.
   //
   ToC.prototype.refresh = function (changedTiddlers) {

      // Bail if `execute()` hasn't been executed or ToC generation is
      // turned off for some reason.
      //
      if (this.skipToC || ! this.tocTiddler) {
         return false;
      }

      const changedAttrs = this.computeAttributes ();
      if ($tw.utils.count (changedAttrs) > 0) {
         this.refreshSelf ();
         return true;
      }

      for (const tiddler of this.allTiddlers) {
         if (changedTiddlers [tiddler]) {
            this.refreshSelf ();
            return true;
         }
      }

      return this.refreshChildren (changedTiddlers);

   };

   exports.tiddlertoc = ToC;

}) ();
