// jshint unused:false

(function () {

   /* eslint no-invalid-this: "off" */
   /* global $tw:             false */

   "use strict";

   const getEnvVar = function (envName, defaultValue) {
      return this.getVariable (envName) ?? defaultValue;
   };

   exports.name   = "ttocid";
   exports.params = [
      {
         "default": "",
         "name"   : "prefix",
      },
   ];

   const cache = {};
   const hash  = function (text) {
      // jshint ignore:start
      cache [text] ??= $tw.utils.hashString (text);
      // jshint ignore:end
      return cache [text];
   };

   exports.run = function (prefix) {

      const getEnv = getEnvVar.bind (this);

      const vars = [
         getEnv ("currentTab",     ""),
         getEnv ("currentTiddler", ""),
         getEnv ("storyTiddler",   ""),
         getEnv ("thisTiddler",    ""),
         getEnv ("transclusion",   ""),
      ];

      const hashed = hash (vars.join ("\0"));

      return `${prefix}-${hashed}`;

   };

}) ();
