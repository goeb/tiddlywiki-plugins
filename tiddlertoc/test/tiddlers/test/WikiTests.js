// jshint ignore:start
// jshint unused:false

// Note: This is basically the same as TiddlyWiki's own test runner
// (`plugins/tiddlywiki/jasmine/run-wiki-based-tests.js`), with some
// style and eslint related modifications, plus it adds the plugin to
// the test wikis and allows to keep the final newline in the test
// tiddlers, as it should be. This file is licensed under a permissive
// BSD 3-clause license, like the original file.
//
// See <https://tiddlywiki.com/#License> for the full license text.

(function () {

   /* eslint complexity:             "off" */
   /* eslint max-lines-per-function: "off" */
   /* eslint no-magic-numbers:       "off" */
   /* global describe:               false */
   /* global it:                     false */
   /* global expect:                 false */
   /* global $tw:                    false */

   "use strict";

   const testTiddlerFilter =
      "[type[text/vnd.tiddlywiki-multiple]" +
      "tag[$:/tags/wiki-test-spec-tiddlertoc]]";

   const plugin     = $tw.wiki.getTiddler ("$:/plugins/gœb/TiddlerToC");
   const piTiddlers = JSON.parse (plugin.fields.text).tiddlers;
   const piTidList  = [];

   for (const piTitle in piTiddlers) {
      if (Object.hasOwn (piTiddlers, piTitle)) {
         piTidList.push (piTiddlers [piTitle]);
      }
   }

   describe ("Wiki-based TiddlerToC tests", () => {

      const readMultiTiddler = function (title) {
         const tiddlers    = [];
         const rawTiddlers = $tw.wiki.getTiddlerText (title).split (
            "\n+\n",
         );
         $tw.utils.each (rawTiddlers, (rawTiddler) => {
            let   fields = Object.create (null);
            const split  = rawTiddler.split (/\r?\n\r?\n/mgu);
            if (split.length >= 1) {
               fields = $tw.utils.parseFields (split [0], fields);
            }
            if (split.length >= 2) {
               fields.text = split.slice (1)
                  .join ("\n\n")
                  .trim ();
            }
            tiddlers.push (fields);
         });
         return tiddlers;
      };

      const createWidgetNode = function (parser, wiki) {
         return wiki.makeWidget (parser);
      };

      const parseText = function (text, wiki, options) {
         return wiki.parseText ("text/vnd.tiddlywiki", text, options);
      };

      const renderWidgetNode = function (widgetNode) {
         $tw.fakeDocument.setSequenceNumber (0);
         const wrapper = $tw.fakeDocument.createElement ("div");
         widgetNode.render (wrapper, null);
         return wrapper;
      };

      const refreshWidgetNode = function (widgetNode, wrapper) {
         widgetNode.refresh (widgetNode.wiki.changedTiddlers, wrapper);
      };

      const tests = $tw.wiki.filterTiddlers (testTiddlerFilter);

      $tw.utils.each (tests, (title) => {
         const tiddler = $tw.wiki.getTiddler (title);
         const stripnl = tiddler.fields ["strip-final-newline"];
         const toconly = tiddler.fields ["toc-only"];
         const impall  = tiddler.fields ["import-all"];
         const logout  = tiddler.fields ["log-output"];
         const logtest = tiddler.fields ["log-test"];
         it (
            `${tiddler.fields.title}: ${tiddler.fields.description}`,
            () => {
               const wiki = new $tw.Wiki ();
               if (impall) {
                  const tts = $tw.wiki.allShadowTitles ();
                  for (const ttl of tts) {
                     const tdd = $tw.wiki.getTiddler (ttl);
                     wiki.addTiddler (tdd);
                  }
               }
               wiki.addTiddlers (piTidList);
               wiki.addTiddlers (readMultiTiddler (title));
               if (! wiki.tiddlerExists ("Output")) {
                  throw new Error ("Missing 'Output' tiddler");
               }
               if (! wiki.tiddlerExists ("ExpectedResult")) {
                  throw new Error ("Missing 'ExpectedResult' tiddler");
               }
               let text = "{{Output}}\n\n";
               if (impall) {
                  const flt = "$:/core/config/GlobalImportFilter";
                  text = `\\import [subfilter{${flt}}]\n\n${text}`;
               }
               const widgetNode = createWidgetNode (
                  parseText (text, wiki),
                  wiki,
               );
               const wrapper = renderWidgetNode (widgetNode);
               wiki.clearTiddlerEventQueue ();
               if (wiki.tiddlerExists ("Actions")) {
                  widgetNode.invokeActionString (
                     wiki.getTiddlerText ("Actions"),
                  );
                  refreshWidgetNode (widgetNode, wrapper);
               }
               let result = wiki.getTiddlerText ("ExpectedResult");
               if (stripnl) {
                  result = result.replace (/(?:\r\n|\n|\r)$/u, "");
               }
               result = result.replace (/\\(?:\r\n|\n|\r)/gu, "");
               let output = wrapper.innerHTML;
               if (logout) {
                  $tw.utils.log (output);
               }
               if (toconly) {
                  output = output.match (
                     /<div class="tiddler-toc[ "].*?<\/div>/gu,
                  ).join ("\n\n");
               }
               if (logtest) {
                  $tw.utils.log ("-- expected --");
                  $tw.utils.log (output);
                  $tw.utils.log ("--  to  be  --");
                  $tw.utils.log (result);
               }
               expect (output).toBe (result);
            },
         );
      });

   });

}) ();
