# Changelog

## [0.0.4] — 2023-09-13

### Fixed

-  Export `varcode` content as plain text on download, don't wikify it.

## [0.0.3] — 2023-09-13

### Added

-  Support download of `varcode` content.

## [0.0.2] — 2023-09-12

### Added

-  Add `class` parameter to `varcode` macro.
-  Add proper title and subtitle to wikis.

## [0.0.1] — 2023-07-05

_Initial release._

[0.0.3]: https://gitlab.com/goeb/tiddlywiki-plugins/-/tree/vc-0.0.3/varcode?ref_type=tags
[0.0.2]: https://gitlab.com/goeb/tiddlywiki-plugins/-/tree/vc-0.0.2/varcode?ref_type=tags
[0.0.1]: https://gitlab.com/goeb/tiddlywiki-plugins/-/tree/vc-0.0.1/varcode?ref_type=tags
