(function () {

   /* eslint no-invalid-this: "off"                 */
   /* eslint strict:          ["error", "function"] */

   "use strict";

   const getEnvVar = function (envName, defaultValue) {
      const envValue = this.getVariable (envName);
      if (typeof envValue === "undefined") {
         return defaultValue;
      }
      return envValue;
   };

   const getFieldValue = function (prefix, fieldName, tiddlerVarName) {
      const tiddlerName = this.getVariable (tiddlerVarName);
      if (typeof tiddlerName !== "undefined") {
         const tiddler = this.wiki.getTiddler (tiddlerName);
         const field   = prefix + fieldName;
         if (
            typeof tiddler !== "undefined" &&
            field in tiddler.fields
         ) {
            return tiddler.getFieldString (field);
         }
      }
      return void 0;
   };

   exports.name = "expandvars";

   exports.params = [
      {
         "default": "",
         "name"   : "text",
      },
      {
         "default": "§(?<name>\\w+)(?:=(?<default>[^§]*))?§",
         "name"   : "pattern",
      },
      {
         "default": "xv-",
         "name"   : "fields",
      },
   ];

   exports.run = function (text, pattern, fields) {

      const getEnv = getEnvVar.bind (this);

      // `cnfPattern` and `cnfPrefix` are set by the `varcode` macro to
      // the values set in the control panel. In case any of these is
      // undefined, use the function parameter value (as set in the
      // `export.params` as the macro parameter defaults).
      //
      const cnfPattern = getEnv ("cnfPattern", pattern);
      const cnfPrefix  = getEnv ("cnfPrefix",  fields );

      // The variables `expandVarsPattern` and `expandVarsFields` take
      // precedence over the control panel settings. Also set the text
      // from the `expandVarsText` variable (defaults to an empty string
      // if the variable isn't set).
      //
      const varText  = getEnv ("expandVarsText",    text      );
      const varRexp  = getEnv ("expandVarsPattern", cnfPattern);
      const varFpfx  = getEnv ("expandVarsFields",  cnfPrefix );

      const regExp   = new RegExp (varRexp, "gu");
      const valCache = new Map ();
      const not      = ["0", "false", "no", "off"];
      let   getField = null;

      if ((! varFpfx && varFpfx !== "") || not.includes (varFpfx)) {
         getField = () => { return void 0; };
      }
      else {
         getField = getFieldValue.bind (this, varFpfx);
      }

      const replace = function (...args) {

         const [matches] = args.slice (-1);

         if (
            typeof matches      !== "object" ||
            typeof matches.name === "undefined"
         ) {
            return "ERROR: Named capture group 'name' missing.";
         }

         if (valCache.has (matches.name)) {
            return valCache.get (matches.name);
         }

         let value = this.getVariable (matches.name);

         // jshint ignore:start
         value ??= getField (matches.name, "storyTiddler"  );
         value ??= getField (matches.name, "currentTiddler");
         value ??= matches.default;
         // jshint ignore:end

         valCache.set (matches.name, value);

         return value;

      };

      return varText.replaceAll (
         regExp,
         (...args) => { return replace.call (this, ...args); },
      );

   };

}) ();
