(function () {

   /* eslint max-lines-per-function: "off" */
   /* eslint new-cap:                "off" */
   /* eslint no-console:             "off" */
   /* eslint no-magic-numbers:       "off" */
   /* eslint quotes:                 "off" */
   /* jshint quotmark:               false */
   /* global beforeEach:             false */
   /* global describe:               false */
   /* global it:                     false */
   /* global jasmine:                false */
   /* global expect:                 false */

   "use strict";

   const wid = require ("$:/core/modules/widgets/widget.js");
   const mod = require ("$:/plugins/gœb/VarCode/modules/ExpandVars.js");

   const run = mod.run.bind (new wid.widget ());

   const txt = mod.params [0].default;
   const rex = mod.params [1].default;
   const pfx = mod.params [2].default;

   describe ("The expandvars macro", () => {

      let tpfx = void 0;

      beforeEach (() => {
         tpfx = (Math.random () + 1).toString (36).substring (7);
      });

      it ("is not null or undefined", () => {
         expect (mod).toEqual (jasmine.anything ());
      });

      it ("has the run method", () => {
         expect (typeof mod.run).toEqual ("function");
      });

      it ("has the expected parameters", () => {
         expect (mod.params [0].name).toEqual ("text"   );
         expect (mod.params [1].name).toEqual ("pattern");
         expect (mod.params [2].name).toEqual ("fields" );
      });

      it ("has an empty string as default text", () => {
         expect (txt).toEqual ("");
      });

      it ("has the default field prefix xv-", () => {
         expect (pfx).toEqual ("xv-");
      });

      it ("handles text without variables", () => {
         expect (run ("X § Z", rex, tpfx)).toEqual ("X § Z");
      });

      it ("ignores invalid variables", () => {
         expect (run ("X §§ Z",    rex, tpfx)).toEqual ("X §§ Z"   );
         expect (run ("X §&=%§ Z", rex, tpfx)).toEqual ("X §&=%§ Z");
         expect (run ("X §=Y§ Z",  rex, tpfx)).toEqual ("X §=Y§ Z" );
      });

      it ("performs basic expansion", () => {
         expect (run ("X §V=Y§ Z", rex, tpfx)).toEqual ("X Y Z");
      });

      it ("replaces undefined variables with undefined", () => {
         expect (run ("X §V§ Z", rex, tpfx)).toEqual ("X undefined Z");
      });

      it ("caches variable lookups", () => {
         expect (run ("X §V=Y§ §V§ Z", rex, tpfx)).toEqual ("X Y Y Z");
         expect (run ("X §V§ §V=Y§ Z", rex, tpfx)).toEqual (
            "X undefined undefined Z",
         );
      });

      it ("works with multiple variables", () => {
         expect (run ("X §A=A§ §B§ §C=C§ Z", rex, tpfx)).toEqual (
            "X A undefined C Z",
         );
      });

      it ("treats variables case-sensitively", () => {
         expect (run ("X §A=A§ §a=B§ Z", rex, tpfx)).toEqual (
            "X A B Z",
         );
      });

      it ("works with multi-line strings", () => {
         expect (run ("\n\nX\n§A=A§\n\n§B=B§\n§A§\n\nZ\n", rex, tpfx))
            .toEqual ("\n\nX\nA\n\nB\nA\n\nZ\n");
      });

      // default pattern is "§(?<name>\\w+)(?:=(?<default>[^§]*))?§"

      it ("works with custom patterns without default", () => {
         const rxp = "#(?<name>[A-Z]+)#";
         expect (run ("X #Y# Z", rxp, tpfx)).toEqual ("X undefined Z");
         expect (run ("X #y# Z", rxp, tpfx)).toEqual ("X #y# Z"      );
         expect (run ("X §y§ Z", rxp, tpfx)).toEqual ("X §y§ Z"      );
      });

      it ("works with custom patterns with default", () => {
         const rxp = "#(?<name>[A-Z]+)(?::(?<default>[^#]*))?#";
         expect (run ("X #Y# Z",   rxp, tpfx)).toEqual (
            "X undefined Z",
         );
         expect (run ("X #y# Z",   rxp, tpfx)).toEqual ("X #y# Z"  );
         expect (run ("X §y§ Z",   rxp, tpfx)).toEqual ("X §y§ Z"  );
         expect (run ("X #Y:y# Z", rxp, tpfx)).toEqual ("X y Z"    );
         expect (run ("X #y:y# Z", rxp, tpfx)).toEqual ("X #y:y# Z");
         expect (run ("X §y=y§ Z", rxp, tpfx)).toEqual ("X §y=y§ Z");
      });

      it ("requires a named capture group", () => {
         const rxp = "#([A-Z]+)#";
         expect (run ("X #Y# Z", rxp, tpfx)).toEqual (
            "X ERROR: Named capture group 'name' missing. Z",
         );
      });

   });

}) ();
